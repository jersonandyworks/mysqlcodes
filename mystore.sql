-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2016 at 02:21 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mystore`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `address`, `phone`) VALUES
(1, 'Billy', 'Crawford', 'Manila, Roxas Street', '29990041'),
(2, 'John', 'Doe', 'USA', '553210'),
(3, 'Timothy Aaron', 'Barrios', 'Davao', '77741`'),
(4, 'Kelly', 'Lambert', '965 Prospect Avenue  Banning, CA 92220', '01089'),
(5, 'Paul', 'Allen', '261 Mulberry Lane  Mays Landing, NJ 08330', '77904'),
(6, 'Elly', 'Octavia', '234 Ivy Lane  Southaven, MS 38671', '15010');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `date`) VALUES
(1, 1, '2016-01-07'),
(3, 2, '2016-01-05'),
(4, 3, '2016-01-03'),
(5, 3, '2015-12-23'),
(6, 6, '2015-12-16'),
(7, 4, '2016-01-21');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `customer_id`, `products_id`, `qty`) VALUES
(1, 1, 2, 1),
(3, 1, 3, 1),
(4, 3, 3, 1),
(6, 6, 7, 2),
(7, 4, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`) VALUES
(1, 'Xbox One', 18000.6, 'blah blah blah'),
(2, 'PlayStation 4', 16000, 'blah blah blah'),
(3, 'Xiaomi 4', 6000, 'blah blah blah'),
(4, 'Lucien Picard Watch', 2000, 'Shattering the notion that watches were fragile, breakable items, G-Shock is your ultimate tough watch. The development of G-Shock is Casio''s response to the challenge of creating a watch that never breaks. Explore the wonder of the great outdoors with G-Shock -- the perfect companion that keeps up with your active lifestyle. '),
(5, 'Invicta Pro Diver', 5000, 'Follow your heart and plunge into that vast sea without any fear while wearing the Invicta Pro Diver Men''s Watch. Built with durability and style in mind, this timepiece is sure to be a favorite diving companion of yours. '),
(6, 'Romoss Sense 6 Plus 20000mAh', 999, ''),
(7, 'Golf GF-200 20,000mAh', 789, 'Capacity : 20000mAh \r\nBattery Cell : Original 18650 Li ion Battery \r\nPower input ( Input ) : DC 5.0V - 1000mA \r\nThere are two outputs that can be used to charge your device simultaneously 2 . \r\nOutput 1 : DC 5.0V - 1000mA ( max ) \r\nOutput 2 : DC 5.0V - 2100mA ( max ) \r\nFlashlight with LED lights \r\n4 indicator lights to determine the level of battery power bank \r\nWith USB cable - Micro USB cable \r\nNot including Power Adapter \r\nSuitable for all types of Tablet / Pad / Smartphone');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
